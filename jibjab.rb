#! /usr/bin/env jruby

$: << File.expand_path(File.dirname(__FILE__))

require 'yaml'
require 'java'
require 'smackx.jar'
require 'smack.jar'
require 'sasl.jar'

java_import org.jivesoftware.smack.ConnectionConfiguration
java_import org.jivesoftware.smack.SASLAuthentication
java_import org.jivesoftware.smack.XMPPConnection
java_import org.jivesoftware.smack.XMPPException
java_import org.jivesoftware.smack.PacketListener
java_import org.jivesoftware.smack.packet.Message
java_import org.jivesoftware.smack.packet.Presence
java_import org.jivesoftware.smack.sasl.FacebookSASLDigestMD5Mechanism
java_import org.jivesoftware.smack.sasl.SASLMechanism
java_import org.jivesoftware.smackx.MessageEventManager
java_import org.jivesoftware.smackx.MessageEventNotificationListener
java_import org.jivesoftware.smackx.MessageEventRequestListener

class JibJab
  def initialize(cfg='config.yml')
    yaml = YAML.load(IO.read(cfg))
    @config = yaml['default'].merge yaml[ENV['PROFILE'] || 'default']
    @config['silent'] = !ARGV.empty?
    @recipient = nil
    @quit = nil
  end

  def connect
    XMPPConnection.DEBUG_ENABLED = true if ENV['DEBUG'] or @config['debug']
    config = ConnectionConfiguration.new @config['host'], @config['port'], @config['service']
    config.sasl_authentication_enabled = @config['sasl']
    if @config['port'] == 5223
    config.security_mode = ConnectionConfiguration::SecurityMode.enabled
    config.socket_factory = DummySSLSocketFactory.new
    end
    @connection = XMPPConnection.new config
    sleep 0.2
    @event_manager = MessageEventManager.new @connection
    @event_manager.add_message_event_request_listener self
    @event_manager.add_message_event_notification_listener self

    begin
      @connection.connect
      SASLAuthentication.registerSASLMechanism 'DIGEST-MD5', FacebookSASLDigestMD5Mechanism.java_class
      SASLAuthentication.supportSASLMechanism  'DIGEST-MD5'
      @connection.login @config['username'], [@config['password']].pack("H*")
      @connection.add_packet_listener self, lambda { |packet| true } unless @config['silent']
      if @connection.connected? and not @config['silent']
        println "[Connected to #{@connection.service_name} with user #{@connection.user}]"
        prompt
      end
      @connection.send_packet Presence.new Presence::Type.available
    rescue XMPPException => ex
      println "[#{ex}]"
    end
    self
  end

  def disconnect
    @connection.send_packet Presence.new Presence::Type.unavailable
    @connection.disconnect if @connection
  end

  def message(to, text)
    return if text.empty?
    msg = Message.new
    MessageEventManager.add_notifications_requests msg, true, true, true, true
    msg.to = @recipient || "#{to}@#{@config['service']}"
    msg.body = text
    @connection.send_packet msg
    @recipient ||= msg.to
  rescue XMPPException => ex
    println "[Error delivering message]"
  end

  def listeners
    @listeners ||= [ lambda { |msg| $stdout.write msg; $stdout.flush } ]
  end

  include PacketListener
  def processPacket(packet)
    if packet.is_a? Presence
      println "#{$/}[Online: #{packet.from} ( %s%s ) ]" % [ packet.away? ? 'away' : packet.type, packet.status.empty? ? '' : " - #{packet.status}"]; prompt
    elsif packet.is_a? Message and not packet.body.empty?
      println "#{$/}#{packet.from}: #{packet.body}"; prompt
    end
  end
  include MessageEventRequestListener
  def deliveredNotificationRequested(from, packetID, messageEventManager); end
  def displayedNotificationRequested(from, packetID, messageEventManager); end
  def composingNotificationRequested(from, packetID, messageEventManager); end
  def offlineNotificationRequested  (from, packetID, messageEventManager); end
  include MessageEventNotificationListener
  def deliveredNotification(from, id); println "#{$/}[Message delivered to #{from}]";  prompt; end
  def displayedNotification(from, id); println "#{$/}[Message displayed for #{from}]"; prompt; end
  def composingNotification(from, id); println "#{$/}[#{from} is typing...]";          prompt; end
  def offlineNotification  (from, id); println "#{$/}[#{from} is offline]";            prompt; end
  def cancelledNotification(from, id); println "#{$/}[#{from} had second thoughts]";   prompt; end

  def prompt
    print "#{$/}#{@recipient}>"
  end

  def println(msg)
    print "#{msg}#{$/}"
  end

  def print(msg)
    listeners.each { |listener| listener.call msg }
  end

  def repeat_every(interval)
    Thread.new { loop { t0 = Time.now; yield; sleep([interval - (Time.now - t0), 0].max) } }
  end

  def run
    message(ARGV.shift, ARGV.join(' ')) or return unless ARGV.empty?
    clock = repeat_every(120) { println "#{$/}[#{Time.now}]"; prompt }
    loop do
      begin
        input = gets.strip
        next if input.empty?
        send(:input_received, input)
      rescue
        println "[#{$!}]" or next
      end
      break if @quit
    end
  ensure
    disconnect
  end

  def input_received(input)
    return prompt if input.empty?
    words = input.split
    command = user = words.first
    command.downcase!
    if command =~ /exit/ or Commands.method_defined? command
      send command, *words[1..-1]
    elsif @recipient # A chat is already in progress
      message @recipient, words.join(' ')
    else
      println "[New chat with #{user} (type 'close' to end)]"
      message user, words[1..-1].join(' ')
    end
    prompt
  end
end # class JibJab

module Commands
  def close(*args)
    if @recipient
      println "[Chat closed: #{@recipient}]"
      @recipient = nil
    end
  end

  def search(*args)
    entry = @connection.roster.get_entry "#{args.first}@#{@config['service']}"
    @connection.roster.remove_entry entry
  end

  def add(*args)
    @connection.roster.create_entry "#{args.first}@#{@config['service']}", '', [ 'Buddies' ]
  end

  def remove(*args)
    entry = @connection.roster.get_entry "#{args.first}@#{@config['service']}"
    @connection.roster.remove_entry entry
  end

  def roster(*args)
    entries = @connection.roster.entries
    if entries.empty?
      println "[Nobody]"
    else
      println "#{entries.map{|a|"#{a.name}: #{a.user}"}.join($/)}"
    end
  end
  alias :list :roster
  alias :who :roster

  def whoami(*args)
    println "#{@connection.user}"
  end

  def quit(*args); @quit = true; end
  JibJab.send :include, self
end

class DummySSLSocketFactory < javax.net.ssl.SSLSocketFactory
  def initialize
    sslcontent = javax.net.ssl.SSLContext.getInstance('TLS')
    sslcontent.init nil, [ OpenTrustManager.new ].to_java(javax.net.ssl.TrustManager), java.security.SecureRandom.new
    @factory = sslcontent.socket_factory
  rescue
  end
  def getDefault; DummySSLSocketFactory.new; end
  def createSocket(*args); @factory.create_socket(*args); end
  def getDefaultCipherSuites; factory.supported_cipher_suites; end
  def getSupportedCipherSuites; factory.supported_cipher_suites; end
end

class OpenTrustManager
  include javax.net.ssl.X509TrustManager
  def initialize; end
  def getAcceptedIssuers; [ java.security.cert.X509Certificate.new ].to_java(java.security.cert.X509Certificate); end
  def checkClientTrusted(arg0, arg1); end
  def checkServerTrusted(arg0, arg1); end
end

if $0 == __FILE__
  JibJab.new.connect.run
end

